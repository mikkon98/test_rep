#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 20
int main()
{
	srand(time(0));
	int arr[N];
	int temp;
	int i, A = 1, B = 200;

	for (i = 0; i < N; i++)
	{
		arr[i] = rand() %(B - A+1) + A;
	}
	printf("Nachalo:\n");
	for (i = 0; i < N; i++)
	{
		printf("%d ", arr[i]);
	}
	puts("\n");
	temp = arr[N - 1];
	for (i = N-1; i >0 ; i--)
	{
		arr[i] = arr[i-1];
	}
	arr[0] = temp;
	printf("Konec:\n");
	for (i = 0; i < N; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}